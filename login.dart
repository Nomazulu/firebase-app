import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'FlutterFire Samples',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.indigo,
        brightness: Brightness.dark,
      ),
      home: const LoginScreen(),
    );
  }
}
class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  // ignore: non_constant_identifier_names
  late Widget LoginForm;

 

  Future<FirebaseApp> _initializeFirebase() async {
    FirebaseApp firebaseApp = await Firebase.initializeApp();

    return firebaseApp;
  }
  @override
  Widget build(BuildContext context) => FutureBuilder(
  future: _initializeFirebase(),
  builder: (context, snapshot) {
    if (snapshot.hasError) {
      return const Text('Error initializing Firebase');
    } else if (snapshot.connectionState ==
        ConnectionState.done) {
      return LoginForm;
    }
    return const CircularProgressIndicator(
      valueColor: AlwaysStoppedAnimation<Color>(
        Colors.blueGrey
      ),
    );
  },
);
}